package dto.user.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DtoUser {
  private String firstName;
  private String lastName;
  private String password;
  private Integer userStatus;
  private String phone;
  private Integer id;
  private String email;
  private String username;
}