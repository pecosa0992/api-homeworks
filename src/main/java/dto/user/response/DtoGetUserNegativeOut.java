package dto.user.response;

import lombok.Data;

@Data
public class DtoGetUserNegativeOut {
  private Integer code;
  private String type;
  private String message;
}